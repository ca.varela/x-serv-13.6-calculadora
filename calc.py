def sumar(a: float, b: float) -> float:
    return a+b

def restar (a: float, b: float) -> float:
    return a-b


print("1 + 2 = ", sumar(1, 2))
print("3 + 4 = ", sumar(3, 4))
print("5 - 6 = ", restar(5, 6))
print("7 - 8 = ", restar(7, 8))


